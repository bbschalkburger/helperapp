class ApplicationController < ActionController::Base
    helper_method :authorize
    helper_method :current_user
    helper_method :logged_in?

    rescue_from ActionController::InvalidAuthenticityToken, with: :user_not_authorized
    
    def user_not_authorized
        redirect_to root_path
    end

    def logged_in?
        !current_user.nil?
    end

    def redirect_if_logged_in?
        redirect_to order_index_path if logged_in?
    end

    def set_cart
        session[:CartItems] = current_user.get_cart_items(session[:ShoppingCartID])
        session[:ShoppingCartNoOfItems] = session[:CartItems]["recordcount"]
    end

    def current_user
        @current_user ||= begin
            User.new(session[:user]) if session[:user]
        end
    end

    def authorize
        return if logged_in?
    end

    def set_current_user(user)
        session[:user] = user # Unchanged sessions
        time = Time.now
        needByDate = "#{time.year}-#{time.month + 2}-#{time.day}"
        cart_params = { UserID: user["userid"], NeedByDate: needByDate, Comments: "" }

        session[:ShoppingCart] = current_user.create_cart(cart_params) # Once, Unchanged
        session[:ShoppingCartID] = session[:ShoppingCart]["ID"] # Unchanged

        session[:ShoppingCartNoOfItems] = "0"
    end
end