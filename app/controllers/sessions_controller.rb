class SessionsController < ApplicationController
  def index
    respond_to do |format|
      format.html
      begin

        user = get_auth_key(params)

        format.html{}
        if !user["Error"].present?
          set_current_user(user)
          redirect_to items_path
        else
          flash[:alert] = "Authentication Key Invalid"
        end
      rescue StandardError
        format.json { render json: { error: 'ERROR' }, status: 401 }
      end
    end
  end

  def create
  end

  def new
  end

  def destroy
    reset_session
    redirect_to root_path
  end

  private

  def require_params
    params.permit(:key, :authenticity_token)
  end

  def get_auth_key(params)
    body = "{\n\t\"punchoutkey\": \"#{params[:key]}\"\n}"
    response = User.http_request(ENV['GET_USER_AUTH'], User.request_type[:post], body)
    response
  end
end
