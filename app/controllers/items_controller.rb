class ItemsController < ApplicationController
  before_action :authorize, :set_cart
  
  def index
    @items = current_user.items(session[:user])["PurchaseableItems"]
  end

  def show
    @cart_items = session[:CartItems]
    @xml = current_user.return_to_coupa( session[:user], @cart_items["ShoppingCartItems"]) if @cart_items["ShoppingCartItems"].present?

    respond_to do |format|
      format.js { render layout: false } 
      # Add this line to you respond_to block
    end
  end

  def create
    @cart_item = current_user.create_cart_item(items_params)
    render :json => { error: false, data: @cart_item }
  end

  def edit
  end

  def cart_catalogue
    @cart_items = session[:CartItems]
    @xml = current_user.return_to_coupa( session[:user], @cart_items["ShoppingCartItems"]) if @cart_items["ShoppingCartItems"].present?
  end

  def view_more
    @cart_items = session[:CartItems]
    @item = current_user.item_get(params, session[:ShoppingCartID])
    @item["FileGUID"] = params[:file]
  end
  
  def increment_quantity
    @cart_item = current_user.update_cart_item(items_update)
    render :json => { error: false, data: @cart_item }
  end

  def decrement_quantity
    @cart_item = current_user.update_cart_item(items_update)
    render :json => { error: false, data: @cart_item }
  end

  def destroy
    @cart_item = current_user.delete_cart_item(items_params_delete)
    render :json => { error: false, data: @cart_item }
  end

  private

  def items_params
    params.permit(:shoppingCartID, :contractItemID, :quantity)
  end

  def items_params_delete
    params.permit(:shoppingCartID, :shoppingCartItemID)
  end

  def items_update
    params.permit(:ShoppingCartID, :shoppingCartItemID, :quantity)
  end
end