require 'uri'
require 'json'
require 'net/http'
require 'dotenv'
require 'digest/md5'

require_relative './concerns/cart_item.rb'
require_relative './concerns/Cart.rb'

class User
    include CartItem
    include Cart

    def initialize(params)
        @id = params['id'] || params['userid']
        @email = params['email']
        @password = params['password']
    end

    def self.http_request(uri, type, body = nil)
        url = URI(uri)

        https = Net::HTTP.new(url.host, url.port)
        https.use_ssl = true

        request_string =  "Net::HTTP::#{type}.new(url)"
        request = eval(request_string)

        request["Content-Type"] = 'application/json'
        request["Site"] = 'NewSupplierStoreUI'
        request['UserID'] = 326
        request.body = body if body.present?

        response = https.request(request)
        JSON.parse(response.read_body)
    end

    def self.request_type
        {
            post: :Post,
            get: :Get,
            put: :Put,
            delete: :Delete
        }
    end

    def get_auth_key(params)
        body = "{\n\t\"punchoutkey\": \"#{params[:key]}\"\n}"
        response = User.http_request(ENV['GET_USER_AUTH'], User.request_type[:post], body)
        response
    end

    def items(user)
        User.http_request(
            "#{ENV['ITEMS_FOR_USER']}/#{user["userid"]}/PurchaseableItems?ContractID=#{user["ContractID"]}",
            User.request_type[:get]
        )
    end

    def item_get(params, shoppingCartID)
        User.http_request(
            "#{ENV['EACH_ITEM']}/#{params[:id]}",
            User.request_type[:get]
        )
    end
end