module CartItem
    def create_cart_item(params)
        body = "{\n   \"contractItemID\":#{params[:contractItemID]},\n   \"Quantity\":#{params[:quantity]}\n}"
        response = User.http_request("#{ENV['SHOPPING_CART']}/#{params[:shoppingCartID]}/Item", User.request_type[:post], body)
        response
    end
    
    def get_cart_items(shoppingCartID)
        response = User.http_request("#{ENV['SHOPPING_CART']}/#{shoppingCartID}/Item", User.request_type[:get])
    end

    def update_cart_item(params)
        body = "{\n    \"quantity\":#{params[:quantity].to_i}\n}"
        response = User.http_request("#{ENV['SHOPPING_CART']}/#{params[:ShoppingCartID].to_i}/Item/#{params[:shoppingCartItemID].to_i}", User.request_type[:put], body)
        response
    end

    def delete_cart_item(params)
        response = User.http_request("#{ENV['SHOPPING_CART']}/#{params[:shoppingCartID]}/Item/#{params[:shoppingCartItemID]}", User.request_type[:delete])
        response
    end

    def return_to_coupa(user, items)
        total = 0
        items.each do |item| total += item["ContractItem-Price"]*item["Quantity"] end
        body = "<cXML payloadID='#{Time.now.to_i}' xml:lang='en-US' timestamp='#{Time.now.strftime("%F | %T")}' version='1.2.0.14'>
            <Header>
            <From>
                <Credential domain='NetworkID'>
                <Identity/>
                </Credential>
            </From>
            <To>
                <Credential domain='NetworkId'>
                <Identity>user@coupa.com</Identity>
                </Credential>
            </To>
            <Sender>
                <Credential domain='NetworkID'>
                <Identity/>
                </Credential>
                <UserAgent/>
            </Sender>
            </Header>
            <Message>
            <PunchOutOrderMessage>
                <BuyerCookie>#{user["BuyerCookie"]}</BuyerCookie>
                <PunchOutOrderMessageHeader operationAllowed='edit'>
                <Total>
                    <Money currency='ZAR'>#{total}</Money>
                </Total>
                </PunchOutOrderMessageHeader>"
            items.each do |item|
            body +=       "<ItemIn quantity='#{item["Quantity"]}'>
                            <ItemID>
                                <SupplierPartID>#{item["ProductCode"]}</SupplierPartID>
                            </ItemID>
                            <ItemDetail>
                                <UnitPrice><Money currency='ZAR'>#{item["ContractItem-Price"]}</Money></UnitPrice>
                                <Name xml:lang='en-US'>#{item["Name"]}</Name>
                                <Description xml:lang='en-US'>#{item["Name"]}</Description>
                                <UnitOfMeasure>EA</UnitOfMeasure>
                                <Classification domain='UNSPSC'>41106104</Classification>
                                <ManufacturerName/>
                                <LeadTime>0</LeadTime>
                            </ItemDetail>
                            </ItemIn>"
                    end
            body +=     "</PunchOutOrderMessage>
                    </Message>
                    </cXML>"
    end
end