module Cart
    def create_cart(params)
        body = "{\n    
            \"UserID\":#{params[:UserID]},\n    
            \"NeedByDate\":\"#{params[:NeedByDate]}\",\n    
            \"Comment\":\"#{params[:Comments]}\" \n
        }"
        response = User.http_request(ENV['SHOPPING_CART'], User.request_type[:post], body)
        response
    end

    def get_cart(shoppingCartID)
        response = User.http_request("#{ENV['SHOPPING_CART']}/#{shoppingCartID}")
    end
end