$(document).on("turbolinks:load", function () {
  var current_user, cart_items = '';
  var cart = '';

  $(".add_to_cart").click(function(e){
    e.preventDefault();
    if ($(this).hasClass("disabled")) return;
    $(this).addClass("disabled");
    current_user = $(this).data("user");
    cart = $(this).data("shoppingcart"); // always in small letters
    $(this).html("Item added");
    if(cart) add_item_into_cart(this, current_user);
  });

  $('.remove_link').click(function(e){
    e.preventDefault()
    var shoppingCart = $(this).data("shoppingcart");
    var shoppingCartID = shoppingCart["ID"];
    var shoppingCartItemID = $(this).data("shoppingcartitemid")
    let elementWrapper = document.getElementById("shopping-cart__item--" + shoppingCartItemID)
    delete_in_cart(this, shoppingCartID, shoppingCartItemID);
  });

  $(".decrement").click(function(e){
    e.preventDefault()
    var shoppingCartItemID = $(this).data("shoppingcartitemid")
    cart = $(this).data("shoppingcart")
    var elementWrapper = document.getElementById("quantity-input--"+ shoppingCartItemID)
    var inputValue = elementWrapper.value
    decrement_item_quantity(this, inputValue, shoppingCartItemID)
  });

  $(".increment").click(function(e){
    e.preventDefault()
    var shoppingCartItemID = $(this).data("shoppingcartitemid")
    cart = $(this).data("shoppingcart")
    var elementWrapper = document.getElementById("quantity-input--"+ shoppingCartItemID)
    var inputValue = elementWrapper.value    
    increment_item_quantity(this, inputValue, shoppingCartItemID)
  });

  // ---------------------------------------------------------------
  // ---------------------------------------------------------------
  // ---------------------------------------------------------------

  // Version 1
  function decrement_item_quantity(button, quantity, shoppingCartItemID){
    var requestURL = `https://tendertiger.azure-api.net/ShoppingCart/{{ShoppingCartID}}/Item/{{ShoppingCartItemID}}`
    requestURL = '/decrement'

    if (quantity > 1){
      $.ajax({
        url: requestURL,
        method: "PUT",
        contentType: "application/json; charset=UTF-8",
        data: JSON.stringify({ "ShoppingCartID": cart["ID"], "shoppingCartItemID": shoppingCartItemID, "quantity": parseInt(quantity) - 1 }),
        beforeSend: function (xhr) { xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},

        success: function (resp) {
          if (resp.error){
            show_error(resp.message);
            $(button).text("Item already added");
          }
          else {
            cart_items = resp.data;
            $("#quantity-input--"+shoppingCartItemID).val(parseInt(quantity) - 1)            
            // ($(".quantity-input--"+ shoppingCartItemID).value() === 1) ?  $(".quantity-input--"+ shoppingCartItemID).text(1) : $(".quantity-input--"+ shoppingCartItemID).text(parseInt($(".quantity-input--"+ shoppingCartItemID).text()) - 1);
          }
        },
        async: true,
      });
    }
  }

  function increment_item_quantity(button, quantity, shoppingCartItemID){
    var requestURL = `https://tendertiger.azure-api.net/ShoppingCart/{{ShoppingCartID}}/Item/{{ShoppingCartItemID}}`
    requestURL = '/increment'

    $.ajax({
      url: requestURL,
      method: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify({ "ShoppingCartID": cart["ID"], "shoppingCartItemID": shoppingCartItemID, "quantity": parseInt(quantity) + 1 }),
      beforeSend: function (xhr) { xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},

      success: function (resp) {
        if (resp.error){
          show_error(resp.message);
          $(button).text("Item already added");
        }
        else {
          cart_items = resp.data;  
          $("#quantity-input--"+shoppingCartItemID).val(parseInt(quantity) + 1)
        }
      },
      async: false,
    });
  }

  function add_item_into_cart(button, current_user){
    var item_id = $(button).data("itemid");
    var requestURL = `https://tendertiger.azure-api.net/ShoppingCart/`+cart["ID"]+`/Item`; 
    requestURL = "/items";

    $.ajax({
      url: requestURL,
      method: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify({ "shoppingCartID": cart["ID"], "contractItemID": item_id, "quantity": 1 }),
      beforeSend: function (xhr) { xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},

      success: function (resp) {
        if (resp.error){
          show_error(resp.message);
          $(button).text("Item already added");
        }
        else {
          cart_items = resp.data;
          $(button).text("Item added");

          ($(".cart-quantity").text() === "0") ?  $(".cart-quantity").text(1) : $(".cart-quantity").text(parseInt($(".cart-quantity").text()) + 1);
        }
      },
      async: true,
    });
  }

  function delete_in_cart(button, shoppingCartID, shoppingCartItemID){
    var requestURL = "/items/" + shoppingCartItemID 

    $.ajax({
      url: requestURL,
      method: "DELETE",
      data: { shoppingCartID: shoppingCartID, shoppingCartItemID: shoppingCartItemID },
      beforeSend: function (xhr) { xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},

      success: function(resp){
        if (resp.error) console.log(resp.message);
        else{
          console.log(resp.data);
          $("#shopping-cart__item--" + shoppingCartItemID).remove();

          ($(".cart-quantity").text() === "1") ?  $(".cart-quantity").text(0) : $(".cart-quantity").text(parseInt($(".cart-quantity").text()) - 1);
          return;
        }
      },
      async: true
    });
  }

  function show_error(message) {
    var error = `<div class="alert alert-danger"> ${message} </div>`;
    $(".page-breadcrumb").after($(error));
    setTimeout(function () { $(".alert").remove(); }, 5000);
  }
});

// console.log($(".quantity-input--"+ shoppingCartItemID).text() === "1") 
// ?  $(".quantity-input--"+ shoppingCartItemID).text(1) : $(".quantity-input--"+ shoppingCartItemID).text(parseInt($(".quantity-input--"+ shoppingCartItemID).text()) + 1);
// if ( $(".quantity-input--"+ shoppingCartItemID).val() === "1") {
//   // $(".quantity-input--"+ shoppingCartItemID).value = 1;
//   console.log("WEIRD WE WE WE WILL WILL ROCK YOU")
// }else{
//   // console.log("WEIRD NUMBER")
//   // $(".quantity-input--"+ shoppingCartItemID).value = parseInt(quantity) + 1 ; // text(parseInt($(".quantity-input--"+ shoppingCartItemID).text()) - 1);
// }
// Version 2
// function decrement_item_quantity(button, quantity, shoppingCartID, shoppingCartItemID){
//   var requestURL = `https://tendertiger.azure-api.net/ShoppingCart/${ShoppingCartID}/Item/${ShoppingCartItemID}`

//   $.ajax({
//     url: requestURL,
//     method: "PUT",
//     contentType: "application/json; charset=UTF-8",
//     data: JSON.stringify({ "quantity": quantity }),
//     beforeSend: function (xhr) { xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},

//     success: function (resp) {
//       if (resp.error){
//         show_error(resp.message);
//         $(button).text("Item already added");
//       }
//       else {
//         cart_items = resp.data;
//         // $(button).text("Item added");

//         // ($(".cart-quantity").text() === "0") ?  $(".cart-quantity").text(1) : $(".cart-quantity").text(parseInt($(".cart-quantity").text()) + 1);
//       }
//     },
//     async: true,
//   });
// }

// var elementWrapper = document.getElementById("quantity-input--"+ shoppingCartItemID)
// var inputValue = elementWrapper.value


// console.log("-------------HELLO----------------")
// console.log("-------------HELLO----------------")
// console.log("-------------HELLO----------------")
// // console.log(elementWrapper)
// // console.log(cart)
// console.log("CART DETAILS")
// console.log(cart)
// console.log("CART ITEM DETAILS")
// console.log(shoppingCartItemID)
// console.log("CART ITEM DETAILS")
// console.log(cart)

// // decrement_item_quantity(this, )
// console.log("-------------HELLO----------------")
// console.log("-------------HELLO----------------")
// console.log("-------------HELLO----------------")
// console.log(elementWrapper)
// console.log("INPUT VALUE")
// console.log(inputValue)
// console.log("CART DETAILS")
// console.log(cart)
// console.log("CART ITEM DETAILS")
// console.log(shoppingCartItemID)


// console.log("-------------HELLO----------------")
// console.log("-------------HELLO----------------")
// console.log("-------------HELLO----------------")
// console.log(elementWrapper)
// console.log("INPUT VALUE")
// console.log(inputValue)
// console.log("CART DETAILS")
// console.log(cart)
// console.log("CART ITEM DETAILS")
// console.log(shoppingCartItemID)

// console.log("------------CART ITEMS IN THE INCREMENT QUANTITY FUNCTION--------------")
// console.log("------------CART ITEMS IN THE INCREMENT QUANTITY FUNCTION--------------")
// console.log("------------CART ITEMS IN THE INCREMENT QUANTITY FUNCTION--------------")
// console.log("------------CART ITEMS IN THE DECREMENT QUANTITY FUNCTION--------------")
// console.log("------------CART ITEMS IN THE DECREMENT QUANTITY FUNCTION--------------")
// console.log("------------CART ITEMS IN THE DECREMENT QUANTITY FUNCTION--------------")
// console.log(cart_items) 
// var elementWrapper = document.getElementById("quantity-input--"+ shoppingCartItemID)
// var inputValue = elementWrapper.value