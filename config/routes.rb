Rails.application.routes.draw do
  root to: 'sessions#index'
  
  get 'new', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  post 'send_coupa', to: 'items#send_coupa'
  post 'shopping_cart', to: 'items#shopping_cart'
  
  get 'cart_catalogue', to: 'items#cart_catalogue'
  get ':id/show', to: 'items#view_more', as: 'view_more'

  put 'decrement', to: 'items#decrement_quantity'
  put 'increment', to: 'items#increment_quantity'

  # delete 'delete_item', to: 'items#delete_item'
  
  resources :items
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end